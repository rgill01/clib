/****************************************************************************
	Vector library, taken from my nuttx app lib. 
	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __INCLUDE_CLIB_DYNAMICS_H_
#define __INCLUDE_CLIB_DYNAMICS_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "vectord.h"
#include <gsl/gsl_rng.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/****************************************************************************
 * Public Types
 ****************************************************************************/

/* everything is in SI units */

/* Dynamics related */

typedef struct {
	double 	mass;
	mat3d  	inertia;	/* assume diagnoal inertia matrix..*/
} dynamics_rigid_body_s;

typedef struct {
	double inertia;
	double K,									/* motor constant */ 
				 R,									/* armature resistance */
				 torque_stiction, 	/* stiction offset */
				 u_max,							/* max voltage */
				 u_min;
} dynamics_motor_s;

typedef struct {
	vec3d pos;  /*  inertial frame */
	vec3d vel;
	rotd  q;		/* forward frame rotation */
	vec3d w;    /* body frame */
} dynamics_rb_state_s;

/* aerodynamics related */

typedef struct {
	double rho, R;

	/* thrust coefs, from leishman notation */
	double ct_0;   			/* T / (0.5*rho*A*(omega*R)**2) */
	double ct_mu2, ct_lc, ct_lc2;   /* Ct = Ct0 + kmu2 + klc + klc**2 */

	/* rotor drag */
	double crd_mu;

	/* torque */
	double cq_0;
	double cq_mu2, cq_lc, cq_lc2;

} aero_prop_s;

typedef struct {
	double rho, c, d;

	/* lift coefs, std notation */
	double cla[3], clb[3]; //x_j[2]; /* piecewise linear */

	/* drag coefs */
	// double cd0, cda;  /* cd0 + cda*sin**2 */
	double cda[2], cdb[2];

	/* moment coefs */ 
	double cma;       /* cma*sin */
} aero_annular_wing_s;

typedef struct {
	double std; /* continuous time std = discrete std / measured sampling gtime */
	double tau; /* continuous time tau = measured sampling time / (1 - AR{a}) */
} aero_turbulence_ar_s;

/****************************************************************************
 * Public Data
 ****************************************************************************/
/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/**
 * dynamics_rigid_body: 
 * 	standard rigid body dynamics given force and torques
 *  pos, vel, and rates are all w.r.t. center of mass
 * @q: forward frame rotation (rotates frame I to B)
 */
void dynamics_rigid_body(const dynamics_rigid_body_s *rb,
	const dynamics_rb_state_s *x, const vec3d *BF, const vec3d *Btau,
	dynamics_rb_state_s *xD);

/**
 * dynamics_motor: 
 */
void dynamics_motor(const dynamics_motor_s *m,
	double w, double u_volts, double torque_load, double *wd);

/**
 * aero_propeller_force_torque
 *  @B_vinf: air velocity (or negated body velocity if no wind) in the 
 * 					body frame, where z aligns with rotor plane
 */
void aero_propeller_force_torque(const aero_prop_s *p, 
	const vec3d *B_Vinf, double Omega, vec3d *BF, double *torque);

/**
 * aero_annular_wing_loads:
 * 	@B_vinf: air velocity (or negated body velocity if no wind) in the 
 * 					body frame
 *  @cf_turb: normalized additive disturbance/turbulence (lift, drag, moment)
 */
void aero_annular_wing_loads(const aero_annular_wing_s *aw,
	const vec3d *B_Vinf, const double (*cf_turb)[3], vec3d *BF, vec3d *BTau);

/**
 * aero_turbulence_dynamics: 
 * 	continous time dynamics function for turbulence and aeroelastics, 
 *  modeled simply as coloured noised by a first-order differential equation 
 *  driven by zero-mean, unit-variance noise eta.
 *  @turb_state: state of the turbulence 
 *  @variate of eta, unit-variance, zero-mean
 */
double aero_turbulence_dynamics(const aero_turbulence_ar_s *at, 
	double turb_state, double eta);

#endif /* #ifndef __INCLUDE_APPS_LIB_UTILITIES_H_ */