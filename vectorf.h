/****************************************************************************
	Vector library
	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

/* all results are given by caller via automatic storage, passed as last arg 
 * some functions are alias safe and others are not! ex.
 * add, add s mult, subtract, are alias safe

 * will pass array pointers into functions.
 * http://eli.thegreenplace.net/2009/10/21/are-pointers-and-arrays-equivalent-in-c
 * 
 * matrices are implemented row-contiguous
 */

#ifndef __INCLUDE_APPS_LIB_VECTOR_H_
#define __INCLUDE_APPS_LIB_VECTOR_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include "utilities.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define _vecinfo(format,...)  printf(format, ##__VA_ARGS__)

/****************************************************************************
 * Vectors
 ****************************************************************************/

#define VECTOR_DEFINE_COMMON_VEC(n,t,type)\
typedef type vec##n##t [n]; \
static inline vec##n##t * vec##n##t##_add(const vec##n##t *a, const vec##n##t *b, vec##n##t *r) { \
	for (uint8_t i=0; i<n; i++) { \
		(*r)[i] = (*a)[i] + (*b)[i];\
	}	\
	return r;\
}\
static inline vec##n##t * vec##n##t##_mult_scalar(const vec##n##t *a, type b, vec##n##t *r) {\
	for (uint8_t i=0; i<n; i++)	{\
		(*r)[i] = (*a)[i]*b;\
	}\
	return r;\
}\
static inline vec##n##t * vec##n##t##_div_scalar(const vec##n##t *a, type b, vec##n##t *r){\
	for (uint8_t i=0; i<n; i++)	{\
		(*r)[i] = (*a)[i]/b;\
	}\
	return r;\
}\
static inline vec##n##t * vec##n##t##_subtract(const vec##n##t *a, const vec##n##t *b, vec##n##t *r){\
	for (uint8_t i=0; i<n; i++)	{\
		(*r)[i] = (*a)[i] - (*b)[i];\
	}\
	return r;\
}\
static inline type vec##n##t##_norm_sq(const vec##n##t *a){\
	type ret = 0;\
	for (uint8_t i=0; i<n; i++)	{\
		ret += (*a)[i]*(*a)[i];\
	}\
	return ret;\
}\
static inline type vec##n##t##_norm(const vec##n##t *a) {\
	return sqrt##t (vec##n##t##_norm_sq(a));\
}\
static inline vec##n##t * vec##n##t##_normalize(vec##n##t *v)\
{\
	type norm = vec##n##t##_norm(v);\
	if (norm > 0)	{\
		for (uint8_t i=0; i<n; i++)\
			(*v)[i] /= norm;\
	}\
	return v;\
}\
static inline type vec##n##t##_inner(const vec##n##t *a, const vec##n##t *b){\
	type ret = 0;\
	for (uint8_t i=0; i<n; i++)	{\
		ret += (*a)[i]*(*b)[i];\
	}\
	return ret;\
}\
static inline vec##n##t * vec##n##t##_mult(const vec##n##t *a, const vec##n##t *b, vec##n##t *r){\
	for (uint8_t i=0; i<n; i++)	{\
		(*r)[i] = (*a)[i]*(*b)[i];\
	}\
	return r;\
}\
static inline void vec##n##t##_print(const char *s, const vec##n##t *a){\
	_vecinfo("%s (", s);\
	for (uint8_t i=0; i<n; i++) \
		_vecinfo("%g ,", (*a)[i]);\
	_vecinfo(")\n");\
}\
static inline vec##n##t * vec##n##t##_copy(const vec##n##t *a, vec##n##t *r){\
	for (uint8_t i=0; i<n; i++)	{\
		(*r)[i] = (*a)[i];\
	}\
	return r;\
}\
static inline vec##n##t * vec##n##t##_initialize(vec##n##t *r, type val){\
	for (uint8_t i=0; i<n; i++)	{\
		(*r)[i] = val;\
	}\
	return r;\
}\
static inline vec##n##t * vec##n##t##_add_s_mult(const vec##n##t *a, const vec##n##t *b, type s, vec##n##t *r){\
	for (uint8_t i=0; i<n; i++)	{\
		(*r)[i] = (*a)[i] + s*(*b)[i];\
	}\
	return r;\
}\

/****************************************************************************
 * Vector 3 specific
 ****************************************************************************/

#define VECTOR_DEFINE_VEC3_SPECIFIC(t,type)\
static inline vec3##t * vec3##t##_cross(const vec3##t *a, const vec3##t *b, vec3##t *r){\
	(*r)[0] = (*a)[1]*(*b)[2] - (*b)[1]*(*a)[2];\
	(*r)[1] = (*b)[0]*(*a)[2] - (*a)[0]*(*b)[2];\
	(*r)[2] = (*a)[0]*(*b)[1] - (*b)[0]*(*a)[1];\
	return r;\
}\
static const vec3##t vec3##t##_zero = { 0 };\
static const vec3##t vec3##t##_i   = { 1, 0, 0 };\
static const vec3##t vec3##t##_j   = { 0, 1, 0 };\
static const vec3##t vec3##t##_k   = { 0, 0, 1 };

/****************************************************************************
 * Matrix
 ****************************************************************************/

#define VECTOR_DEFINE_COMMON_MAT(n,t,type) \
typedef type mat##n##t [n][n]; \
static inline mat##n##t * mat##n##t##_add(const mat##n##t *A, const mat##n##t *B, mat##n##t *R) { \
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++)	{\
			(*R)[i][j] = (*A)[i][j] + (*B)[i][j];\
		}\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_subtract(const mat##n##t *A, const mat##n##t *B, mat##n##t *R){\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++) {\
			(*R)[i][j] = (*A)[i][j] - (*B)[i][j];\
		}\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_add_diag(const mat##n##t *A, type b, mat##n##t *R){\
	for (uint8_t i=0; i<n; i++) {\
		(*R)[i][i] = (*A)[i][i] + b;\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_initialize(mat##n##t *A, type val){\
	for (uint8_t i=0; i<n; i++) {\
		for (uint8_t j=0; j<n; j++) {\
			(*A)[i][j] = val;\
		}\
	}\
	return A;\
}\
static inline mat##n##t * mat##n##t##_diag(mat##n##t *A, type val){\
	for (uint8_t i=0; i<n; i++)	{\
		(*A)[i][i] = val;\
		for (uint8_t j=i+1; j<n; j++)	{\
			(*A)[i][j] = 0;\
			(*A)[j][i] = 0;\
		}\
	}\
	return A;\
}\
static inline mat##n##t * mat##n##t##_transpose(const mat##n##t *A, mat##n##t *R){\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++) {\
			(*R)[i][j] = (*A)[j][i];\
		}\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_mult(const mat##n##t *A, const mat##n##t *B, mat##n##t *R){\
	mat##n##t##_initialize(R,0);\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++)	{\
			for (uint8_t k=0; k<n; k++)	{\
				(*R)[i][j] += (*A)[i][k]*(*B)[k][j];\
			}\
		}\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_mult_transpose(const mat##n##t *A, const mat##n##t *B, mat##n##t *R){\
	mat##n##t##_initialize(R,0);\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++)	{\
			for (uint8_t k=0; k<n; k++)	{\
				(*R)[i][j] += (*A)[i][k]*(*B)[j][k];\
			}\
		}\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_mult_scalar(const mat##n##t *A, type b, mat##n##t *R){\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++) {\
			(*R)[i][j] = (*A)[i][j]*b;\
		}\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_div_scalar(const mat##n##t *A, type b, mat##n##t *R){\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++) {\
			(*R)[i][j] = (*A)[i][j]/b;\
		}\
	}\
	return R;\
}\
static inline mat##n##t * mat##n##t##_copy(const mat##n##t *A, mat##n##t *R){\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++)	{\
			(*R)[i][j] = (*A)[i][j];\
		}\
	}\
	return R;\
}\
static inline void mat##n##t##_print(const char *s, const mat##n##t *a){\
	_vecinfo("%s\n", s);\
	for (uint8_t i=0; i<n; i++)	{\
		_vecinfo("[");\
		for (uint8_t j=0; j<n; j++)	_vecinfo("%g, ", (*a)[i][j]);\
		_vecinfo("]\n");\
	}\
}\
static inline vec##n##t * mat##n##t##_mult_vec##n##t (const mat##n##t *A, const vec##n##t *x, vec##n##t *b){\
	for (uint8_t i=0; i<n; i++)	{\
		(*b)[i] = 0;\
		for (uint8_t j=0; j<n; j++) {\
			(*b)[i] += (*A)[i][j] * (*x)[j];\
		}\
	}\
	return b;\
}\
static inline mat##n##t * mat##n##t##_assign_by_product(mat##n##t *A, const mat##n##t *B) {\
	mat##n##t temp;\
	mat##n##t##_mult(A,B,&temp);\
	mat##n##t##_copy(&temp, A);\
	return A;\
}\
static inline mat##n##t * mat##n##t##_outer(const vec##n##t *a, const vec##n##t *b, mat##n##t *R){\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++)	{\
			(*R)[i][j] = (*a)[i]*(*b)[j];\
		}\
	}\
	return R;\
}\
static inline type mat##n##t##_norm_fro(const mat##n##t *A){\
	type ret = 0;\
	for (uint8_t i=0; i<n; i++)	{\
		for (uint8_t j=0; j<n; j++) {\
			ret += (*A)[i][j]*(*A)[i][j];\
		}\
	}\
	return sqrt##t(ret);\
}\
static const mat##n##t mat##n##t##_zero = { {0} };

/****************************************************************************
 * Matrix 3 specific
 ****************************************************************************/

#define VECTOR_DEFINE_MAT3_SPECIFIC(t,type)\
static const mat3##t mat3##t##_identity = { {1,0,0}, {0,1,0}, {0,0,1} };\
static inline mat3##t * mat3##t##_skew(const vec3##t *v, mat3##t *R) {\
	(*R)[0][0] = (*R)[1][1] = (*R)[2][2] = 0;\
\
	(*R)[0][1] = -(*v)[2];\
	(*R)[1][0] =  (*v)[2];\
	\
	(*R)[0][2] =  (*v)[1];\
	(*R)[2][0] = -(*v)[1];\
\
	(*R)[1][2] = -(*v)[0];\
	(*R)[2][1] =  (*v)[0];\
	return R;\
}

/****************************************************************************
 * Rotation, quaternion representation, assumed to be forward frame rotation
 ****************************************************************************/

#define VECTOR_DEFINE_ROTATION(t,type)\
typedef type rot##t [4];\
static const rot##t rot##t##_identity = { 1, 0, 0, 0 };\
static inline void rot##t##_inverse(const rot##t *q, rot##t *r){\
	/* note, since this is a rotation, we could also just negate the first element
	 * this reduces the number of elements. But doing it this way, as convention
	 * is to keep the first element the same sign as much as possible. 
	 */\
	(*r)[0] = (*q)[0];\
	for (uint8_t i=1; i<4; i++)	{\
		(*r)[i] = -(*q)[i];\
	}\
}\
static inline void rot##t##_mult(const rot##t *q, const rot##t *p, rot##t *r){\
	(*r)[0] = (*q)[0]*(*p)[0] - (*q)[1]*(*p)[1] - (*q)[2]*(*p)[2] - (*q)[3]*(*p)[3];\
	(*r)[1] = (*q)[1]*(*p)[0] + (*q)[0]*(*p)[1] - (*q)[3]*(*p)[2] + (*q)[2]*(*p)[3];\
	(*r)[2] = (*q)[2]*(*p)[0] + (*q)[3]*(*p)[1] + (*q)[0]*(*p)[2] - (*q)[1]*(*p)[3];\
	(*r)[3] = (*q)[3]*(*p)[0] - (*q)[2]*(*p)[1] + (*q)[1]*(*p)[2] + (*q)[0]*(*p)[3];\
}\
static inline void rot##t##_rotate_vector(const rot##t *q, const vec3##t *v, vec3##t *r){\
	vec3##t *qv, temp, temp2;\
	qv = (vec3##t *) &(*q)[1];\
	/* r = <q,v>q */\
	vec3##t##_mult_scalar(qv, vec3##t##_inner(qv, v), r);\
	/* r += q0^2*v */\
	vec3##t##_add_s_mult(r, v, square##t ((*q)[0]), r);\
	/* temp = qXv */\
	vec3##t##_cross(qv, v, &temp);\
	/* r+= 2*q0* qxv */\
	vec3##t##_add_s_mult(r, &temp, 2*(*q)[0], r);\
	/* temp2 = (qxv)xqv */\
	vec3##t##_cross(&temp, qv, &temp2);\
	/* r+= -(qxv)xv */\
	vec3##t##_subtract(r, &temp2, r);\
}\
static inline void rot##t##_body2inertial(const rot##t *q, const vec3##t *a, vec3##t *r){\
	rot##t##_rotate_vector(q, a, r);\
}\
static inline void rot##t##_inertial2body(const rot##t *q, const vec3##t *a, vec3##t *r){\
	rot##t q_inv = {(*q)[0], -(*q)[1], -(*q)[2], -(*q)[3]};\
	rot##t##_rotate_vector(&q_inv, a, r);\
}\
static inline mat3##t* rot##t##_rotation_matrix(const rot##t *q, mat3##t *R){\
	type r0 = (*q)[0]*(*q)[0];\
	type r1 = (*q)[1]*(*q)[1];\
	type r2 = (*q)[2]*(*q)[2];\
	type r3 = (*q)[3]*(*q)[3];\
\
  (*R)[0][0] = r0 + r1 - r2 - r3;\
  (*R)[1][0] = 2*(*q)[1]*(*q)[2] + 2*(*q)[0]*(*q)[3];\
  (*R)[2][0] = 2*(*q)[1]*(*q)[3] - 2*(*q)[0]*(*q)[2];\
\
  (*R)[0][1] = 2*(*q)[1]*(*q)[2] - 2*(*q)[0]*(*q)[3];\
  (*R)[1][1] = r0 - r1 + r2 - r3;\
  (*R)[2][1] = 2*(*q)[2]*(*q)[3] + 2*(*q)[0]*(*q)[1];\
\
  (*R)[0][2] = 2*(*q)[1]*(*q)[3] + 2*(*q)[0]*(*q)[2];\
  (*R)[1][2] = 2*(*q)[2]*(*q)[3] - 2*(*q)[0]*(*q)[1];\
  (*R)[2][2] = r0 - r1 - r2 + r3;\
  return R;\
}\
static inline void rot##t##_initialize(rot##t *q){\
	(*q)[0] = 1;\
	for (uint8_t i=1; i<4; i++)\
		(*q)[i] = 0;\
}\
static inline void rot##t##_from_ypr(type y, type p, type r, rot##t *q){\
	type cr = cos##t(0.5*r);\
  type sr = sin##t(0.5*r);\
\
  type cp = cos##t(0.5*p);\
  type sp = sin##t(0.5*p);\
\
  type cy = cos##t(0.5*y);\
  type sy = sin##t(0.5*y);\
\
  (*q)[0] =  cy*cp*cr + sy*sp*sr;\
  (*q)[1] =  cy*cp*sr - sy*sp*cr;\
  (*q)[2] =  cy*sp*cr + sy*cp*sr;\
  (*q)[3] = -cy*sp*sr + sy*cp*cr;\
}\
static inline void rot##t##_from_yaw(type y, rot##t *q){\
  (*q)[0]  = cos##t(0.5*y);\
  (*q)[1]  = 0;\
  (*q)[2]  = 0;\
  (*q)[3]  = sin##t(0.5*y);\
}\
static inline void rot##t##_from_pitch(type p, rot##t *q){\
  (*q)[0]  = cos##t(0.5*p);\
  (*q)[1]  = 0;\
  (*q)[2]  = sin##t(0.5*p);\
  (*q)[3]  = 0;\
}\
static inline void rot##t##_from_roll(type r, rot##t *q){\
  (*q)[0]  = cos##t(0.5*r);\
  (*q)[1]  = sin##t(0.5*r);\
  (*q)[2]  = 0;\
  (*q)[3]  = 0;\
}\
static inline void rot##t##_from_rotation_vector(const vec3##t *v, rot##t *q){\
	type theta = vec3##t##_norm(v);\
	if (theta >0) {\
		vec3##t##_mult_scalar(v, sin##t(theta/2)/theta, (vec3##t *) &(*q)[1]);\
		(*q)[0] = cos##t(theta/2);\
	} else {\
		(*q)[0] = 1;\
		(*q)[1] = (*q)[2] = (*q)[3] = 0;\
	}\
}\
static inline void rot##t##_to_rotation_vector(const rot##t *q, vec3##t *v){\
	vec3##t##_initialize(v, 0);\
	vec3##t *a = (vec3##t *) &(*q)[1];\
	type sinhalfTh = vec3##t##_norm(a);\
	if (sinhalfTh > 1e-10) {\
		type coshalfTh = (*q)[0];\
		type Th = atan2##t(sinhalfTh, coshalfTh)*2;\
		if (Th > ((type) M_PI)) Th -= 2*((type) M_PI);\
		vec3##t##_mult_scalar(a, Th/sinhalfTh, v);\
	}\
}\
static inline void rot##t##_copy(const rot##t *q, rot##t *r){\
	for (uint8_t i=0; i<4; i++)\
		(*r)[i] = (*q)[i];\
}\
static inline void rot##t##_from_vector_part(const vec3##t *v, rot##t *q){\
	for (uint8_t i=0; i<3; i++)\
		(*q)[i+1] = (*v)[i];\
\
	type mag_sq = vec3##t##_norm_sq(v);\
	/* assume sign of q0 is positive.. */\
	(*q)[0] = (mag_sq < 1) ? sqrt##t (1-mag_sq) : 0;\
}\
static inline void rot##t##_assign_by_product(rot##t *q, const rot##t *p){\
	rot##t temp;\
	rot##t##_mult(q, p, &temp);\
	rot##t##_copy(&temp, q);\
}\
static inline void rot##t##_normalize(rot##t *q){\
  type norm = sqrt##t (square##t ((*q)[0]) + square##t ((*q)[1]) + square##t ((*q)[2]) + square##t ((*q)[3]));\
  for (uint8_t i=0; i<4; i++)\
  	(*q)[i] /= norm;\
}\
static inline void rot##t##_from_unit_a2b(const vec3##t *a, const vec3##t *b, rot##t *q) {\
	/* construct the rotation from vector a to vector b. Assumes a, b are unit vectors
	 * efficient version:
	 * https://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another
	 */	\
	type dot = vec3##t##_inner(a,b);\
	vec3##t *c = (vec3##t *) &(*q)[1];\
	(*q)[0] = 1 + dot;\
	if (dot < -0.9999) { /* if the vectors are parallel but opposite, treat */\
		vec3##t##_cross(a,&vec3##t##_i,c);\
		if (vec3##t##_norm(c) < 0.0001)\
			vec3##t##_cross(a,&vec3##t##_j,c);\
	} else {\
		vec3##t##_cross(a, b, c);\
		rot##t##_normalize(q);\
	}\
}\
static inline void rot##t##_print(const char *s, const rot##t *q) {\
	_vecinfo("%s (%g, %g, %g, %g)\n", s, (*q)[0], (*q)[1], (*q)[2], (*q)[3]);\
}\
static inline void rot##t##_rate(const rot##t *q, const vec3##t *Bw, rot##t *qD){\
	/* qD = 1/2 Q(q) [0 Bw] */\
	double Q[4][3] = { {-(*q)[1], -(*q)[2], -(*q)[3]},\
                     { (*q)[0], -(*q)[3],  (*q)[2]},\
                     { (*q)[3],  (*q)[0], -(*q)[1]},\
                     {-(*q)[2],  (*q)[1],  (*q)[0]} };\
	for (uint8_t i=0; i<4; i++){\
		(*qD)[i] = 0;\
		for (uint8_t j=0; j<3; j++) {\
			(*qD)[i] += Q[i][j] * (*Bw)[j];\
		}\
		(*qD)[i] *= 0.5;\
	}\
}

/****************************************************************************
 * Public Function Definitions
 ****************************************************************************/

VECTOR_DEFINE_COMMON_VEC(3,f,float)
VECTOR_DEFINE_VEC3_SPECIFIC(f,float)

VECTOR_DEFINE_COMMON_MAT(3,f,float)
VECTOR_DEFINE_MAT3_SPECIFIC(f,float)

VECTOR_DEFINE_COMMON_VEC(9,f,float)
VECTOR_DEFINE_COMMON_MAT(9,f,float)

VECTOR_DEFINE_ROTATION(f,float)
#define rotf_old2new 		rotf_inertial2body
#define rotf_new2old 		rotf_body2inertial

/****************************************************************************
 * OpenGL specific
 ****************************************************************************/
#if defined(linux)

VECTOR_DEFINE_COMMON_VEC(4,f,float)
VECTOR_DEFINE_COMMON_MAT(4,f,float)

static const mat4f mat4f_identity = { {1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {0,0,0,1} };

static inline mat4f* mat4f_translate(const vec3f *v, mat4f *R)
{
	(*R)[0][3] = (*v)[0];
	(*R)[1][3] = (*v)[1];
	(*R)[2][3] = (*v)[2];
	return R;
}
static inline mat4f* mat4f_look_at(const vec3f *pos, const vec3f *tar, const vec3f *up, mat4f *R)
{
	/* inverse translation */
	
	mat4f p;
	vec3f pv;
	vec3f_mult_scalar(pos, -1, &pv);
	mat4f_copy(&mat4f_identity, &p);
	mat4f_translate(&pv, &p);

	/* define camera frame in world coordinates */
	/* forward vector */

	vec3f f;
	vec3f_normalize( vec3f_subtract(tar, pos, &f) );	

	/* right vector */

	vec3f r;
	vec3f_normalize( vec3f_cross(&f, up, &r) );

	/* real up vector */

	vec3f u;
	vec3f_normalize( vec3f_cross(&r, &f, &u) );

	mat4f ori; /* forward rotation from camera frame to world frame
	            * transpose the above vectors and put them columnwise..
	            */
	mat4f_copy(&mat4f_identity, &ori);
	ori[0][0] = r[0];
	ori[0][1] = r[1];
	ori[0][2] = r[2];
	ori[1][0] = u[0];
	ori[1][1] = u[1];
	ori[1][2] = u[2];
	ori[2][0] = -f[0];
	ori[2][1] = -f[1];
	ori[2][2] = -f[2]; 

	mat4f_mult(&ori, &p, R);
	return R; 
 }

static inline mat4f* mat4f_perspective(float fov, float ar, float near, float far, mat4f *R)
{
	float range = tanf(fov/2.0) * near;
	float sx    = (2.0 * near)/(2*range*ar);
	float sy    = near / range;
	float sz 		= -(far + near)/(far - near);
	float pz 		= -(2.0 * far * near)/(far - near);
	mat4f_copy(&mat4f_zero, R);
	(*R)[0][0] = sx;
	(*R)[1][1] = sy;
	(*R)[2][2] = sz;
	(*R)[2][3] = pz;
	(*R)[3][2] = -1;
	return R;
}

static inline mat4f* mat4f_ortho(float l, float r, float b, float t, float n, float f, mat4f *R)
{
	(*R)[0][0] = 2.f/(r-l);
	(*R)[1][0] = (*R)[0][2] = (*R)[0][3] = 0.f;

	(*R)[1][1] = 2.f/(t-b);
	(*R)[0][1] = (*R)[1][2] = (*R)[1][3] = 0.f;

	(*R)[2][2] = -2.f/(f-n);
	(*R)[0][2] = (*R)[2][1] = (*R)[2][3] = 0.f;
	
	(*R)[0][3] = -(r+l)/(r-l);
	(*R)[1][3] = -(t+b)/(t-b);
	(*R)[2][3] = -(f+n)/(f-n);
	(*R)[3][3] = 1.f;
	return R;
}

static inline mat4f* mat4f_rotation(const rotf *q, mat4f *T)
{
	mat3f R;
	rotf_rotation_matrix(q, &R);

	mat4f_copy(&mat4f_identity, T);

	for (uint8_t i = 0; i < 3; i ++)
	{
		for (uint8_t j = 0; j < 3; j ++)
		{
			(*T)[i][j] = R[i][j];
		}
	}
	return T;
}

static inline mat4f* mat4f_rot_trans(const rotf *q, const vec3f *v, mat4f *T)
{
	mat4f_rotation(q, T);
	mat4f_translate(v, T);
	return T;
}

static inline mat4f* mat4f_rot_trans_camera
	(const rotf *q, const vec3f *v, const mat4f *view, const mat4f *proj, mat4f *T)
{
	mat4f T1, T2;

	/* first perform world frame to model frame */

	mat4f_rotation(q, &T1);
	mat4f_translate(v, &T1);

	/* camera to world frame */

	mat4f_mult(proj, view, &T2);

	/* camera to model frame */

	mat4f_mult(&T2, &T1, T);

	return T;
}
#endif /* if open GL */

#endif /* #define __INCLUDE_APPS_LIB_VECTOR_H_ */
