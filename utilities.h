/****************************************************************************
	utilities.h
	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __INCLUDE_APPS_LIB_UTILITIES_H_
#define __INCLUDE_APPS_LIB_UTILITIES_H_
/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <math.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* math */

#define UTILITIES_H_SQUARE(t,type)\
 static inline type square##t(type x) { return x*x;}
#define UTILITIES_H_CUBE(t,type)\
 static inline type cube##t(type x) { return x*x*x; }
#define UTILITIES_H_QUARTIC(t,type)\
 static inline type quartic##t(type x) { return x*x*x*x; }
#define UTILITIES_H_QUINTIC(t,type)\
 static inline type quintic##t(type x) { return x*x*x*x*x; }

#define UTILITIES_H_MINMAX(t,type)\
 static inline type min##t(type x, type y)\
{ return (x > y) ? y : x; }\
 static inline type max##t(type x, type y)\
{ return (x > y) ? x : y; }

#define UTILITIES_H_CONSTRAIN(t,type)\
 static inline void constrain##t(type *x, type xmin, type xmax) {\
	if (*x > xmax) 			*x = xmax; \
	else if (*x < xmin) *x = xmin; }

#define UTILITIES_H_SAFEACOS(t,type)\
 static inline type safeacos##t(type x) {constrain##t(&x, -1, 1); return acos##t(x);}

#define UTILITIES_H_SIGN(t,type)\
 static inline type sign##t(type x) {return (x < 0) ? -1 : 1;}

/* other utilities */

#define UTILITIES_DUMMY_PASTER(x,y) x##y
#define UTILITIES_SELECT(x,no) UTILITIES_DUMMY_PASTER(x##_,no)

/****************************************************************************
 * Public Types
 ****************************************************************************/
/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Function Definitions
 ****************************************************************************/

UTILITIES_H_SQUARE(f, float)
UTILITIES_H_CUBE(f, float)
UTILITIES_H_QUARTIC(f, float)
UTILITIES_H_QUINTIC(f, float)
UTILITIES_H_MINMAX(f, float)
UTILITIES_H_CONSTRAIN(f, float)
UTILITIES_H_SAFEACOS(f, float)
UTILITIES_H_SIGN(f, float)
static inline float radiansf(float x) { return x*(((float)M_PI)/180.0f); }
static inline float degreesf(float x) { return x*(180/((float)M_PI)); }

#ifdef linux /* doubles not supported onboard */
UTILITIES_H_SQUARE(d, double)
UTILITIES_H_CUBE(d, double)
UTILITIES_H_CONSTRAIN(d, double)
UTILITIES_H_MINMAX(d, double)
#define acosd acos
UTILITIES_H_SAFEACOS(d, double)
static inline double radiansd(double x) { return x*(M_PI/180); }
static inline double degreesd(double x) { return x*(180/M_PI); }
#endif

UTILITIES_H_MINMAX(ui, unsigned int)

#endif /* #ifndef __INCLUDE_APPS_LIB_UTILITIES_H_ */