/****************************************************************************
	Vector library, taken from my nuttx app lib. 
	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __INCLUDE_APPS_LIB_COM_UTILITIES_H_
#define __INCLUDE_APPS_LIB_COM_UTILITIES_H_
/****************************************************************************
 * Included Files
 ****************************************************************************/
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/****************************************************************************
 * Public Types
 ****************************************************************************/
/****************************************************************************
 * Public Data
 ****************************************************************************/
/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

int comutil_set_attributes(int fd, int speed, int parity, int should_block, int read_size);

int comutil_flush_input(int fd);

#endif /* #ifndef __INCLUDE_APPS_LIB_UTILITIES_H_ */