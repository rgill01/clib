/****************************************************************************
	Vector library
	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#ifndef __INCLUDE_APPS_LIB_VECTOR_DOUBLE_H_
#define __INCLUDE_APPS_LIB_VECTOR_DOUBLE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "vectorf.h" /* just use the macros to define */

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define sqrtd sqrt /* small fix for macros */
#define sind sin 
#define cosd cos
#define atan2d atan2

/****************************************************************************
 * Public Function Definitions
 ****************************************************************************/

VECTOR_DEFINE_COMMON_VEC(3,d,double)
VECTOR_DEFINE_VEC3_SPECIFIC(d,double)

VECTOR_DEFINE_COMMON_MAT(3,d,double)
VECTOR_DEFINE_MAT3_SPECIFIC(d,double)

VECTOR_DEFINE_ROTATION(d,double)
#define rotd_old2new 		rotd_inertial2body
#define rotd_new2old 		rotd_body2inertial

#endif /* #define __INCLUDE_APPS_LIB_VECTOR_H_ */