
struct tmrutil_info 
{
	int timer_fd;
	unsigned long long wakeups_missed;
};


int tmrutil_start (unsigned int period_ms, struct tmrutil_info *info);
void tmrutil_wait (struct tmrutil_info *info);
void tmrutil_close(struct tmrutil_info *info);