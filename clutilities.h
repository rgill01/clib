#ifndef __COMMON_CL_UTIL_H_
#define __COMMON_CL_UTIL_H_

#include <CL/cl.h>
#include <stdbool.h>

/*****************************************************************************
 * Public Types
 ****************************************************************************/

typedef struct {
  cl_device_id      device;
  cl_context        context;
  cl_command_queue  cmd_q;
  cl_program        program;
  cl_kernel         kernel;
} clutil_basic_params_s;

/*****************************************************************************
 * Public API 
 ****************************************************************************/

/** clutil_build_kernel
  * returns: cl_program, must be freed using clReleaseProgram(..)
  */
cl_program clutil_build_program(cl_device_id device, 
                                cl_context context, 
                                const char *kernel_file_path,
                                const char *ccflags);

/** load_device
  *   
  */
cl_device_id clutil_load_device(int dev_number, bool print_info);

/** perror
 * returns: negative if there is an error and prints it if so
 */
int clutil_check_error(cl_int err, int line);
#define clutil_check_error_line(err)  clutil_check_error(cl_int err, __LINE__)

/** setup
 * perform typical setup, calls clutil_load_dev with dev 0
 */
clutil_basic_params_s* clutil_basic_init(const char *kernel_file_path, 
                                         const char *kernel_main,
                                         const char *ccflags);

void clutil_basic_deinit(clutil_basic_params_s **params);

#endif /* include guard */