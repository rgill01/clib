#include "../clutilities.h"
#include <stdio.h>

/*****************************************************************************
 * Preprocessor 
 ****************************************************************************/

#define _clinfo(x...) 	printf("[clutil] " x)
#define MAX_PLATFORMS 10
#define MAX_DEVICES 10

/* for error codes */
#define TO_STRING(x) #x

/*****************************************************************************
 * Private data 
 ****************************************************************************/

static const char* cl_error_strings[] = { 
	TO_STRING(CL_SUCCESS																	),                                 
	TO_STRING(CL_DEVICE_NOT_FOUND                         ),
	TO_STRING(CL_DEVICE_NOT_AVAILABLE                     ),
	TO_STRING(CL_COMPILER_NOT_AVAILABLE                   ),
	TO_STRING(CL_MEM_OBJECT_ALLOCATION_FAILURE            ),
	TO_STRING(CL_OUT_OF_RESOURCES                         ),
	TO_STRING(CL_OUT_OF_HOST_MEMORY                       ),
	TO_STRING(CL_PROFILING_INFO_NOT_AVAILABLE             ),
	TO_STRING(CL_MEM_COPY_OVERLAP                         ),
	TO_STRING(CL_IMAGE_FORMAT_MISMATCH                    ),
	TO_STRING(CL_IMAGE_FORMAT_NOT_SUPPORTED               ),
	TO_STRING(CL_BUILD_PROGRAM_FAILURE                    ),
	TO_STRING(CL_MAP_FAILURE                              ),
	TO_STRING(CL_MISALIGNED_SUB_BUFFER_OFFSET             ),
	TO_STRING(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST ),
	TO_STRING(CL_COMPILE_PROGRAM_FAILURE                  ),
	TO_STRING(CL_LINKER_NOT_AVAILABLE                     ),
	TO_STRING(CL_LINK_PROGRAM_FAILURE                     ),
	TO_STRING(CL_DEVICE_PARTITION_FAILED                  ),
	TO_STRING(CL_KERNEL_ARG_INFO_NOT_AVAILABLE            ),

	TO_STRING(CL_INVALID_VALUE                            ),
	TO_STRING(CL_INVALID_DEVICE_TYPE                      ),
	TO_STRING(CL_INVALID_PLATFORM                         ),
	TO_STRING(CL_INVALID_DEVICE                           ),
	TO_STRING(CL_INVALID_CONTEXT                          ),
	TO_STRING(CL_INVALID_QUEUE_PROPERTIES                 ),
	TO_STRING(CL_INVALID_COMMAND_QUEUE                    ),
	TO_STRING(CL_INVALID_HOST_PTR                         ),
	TO_STRING(CL_INVALID_MEM_OBJECT                       ),
	TO_STRING(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR          ),
	TO_STRING(CL_INVALID_IMAGE_SIZE                       ),
	TO_STRING(CL_INVALID_SAMPLER                          ),
	TO_STRING(CL_INVALID_BINARY                           ),
	TO_STRING(CL_INVALID_BUILD_OPTIONS                    ),
	TO_STRING(CL_INVALID_PROGRAM                          ),
	TO_STRING(CL_INVALID_PROGRAM_EXECUTABLE               ),
	TO_STRING(CL_INVALID_KERNEL_NAME                      ),
	TO_STRING(CL_INVALID_KERNEL_DEFINITION                ),
	TO_STRING(CL_INVALID_KERNEL                           ),
	TO_STRING(CL_INVALID_ARG_INDEX                        ),
	TO_STRING(CL_INVALID_ARG_VALUE                        ),
	TO_STRING(CL_INVALID_ARG_SIZE                         ),
	TO_STRING(CL_INVALID_KERNEL_ARGS                      ),
	TO_STRING(CL_INVALID_WORK_DIMENSION                   ),
	TO_STRING(CL_INVALID_WORK_GROUP_SIZE                  ),
	TO_STRING(CL_INVALID_WORK_ITEM_SIZE                   ),
	TO_STRING(CL_INVALID_GLOBAL_OFFSET                    ),
	TO_STRING(CL_INVALID_EVENT_WAIT_LIST                  ),
	TO_STRING(CL_INVALID_EVENT                            ),
	TO_STRING(CL_INVALID_OPERATION                        ),
	TO_STRING(CL_INVALID_GL_OBJECT                        ),
	TO_STRING(CL_INVALID_BUFFER_SIZE                      ),
	TO_STRING(CL_INVALID_MIP_LEVEL                        ),
	TO_STRING(CL_INVALID_GLOBAL_WORK_SIZE                 ),
	TO_STRING(CL_INVALID_PROPERTY                         ),
	TO_STRING(CL_INVALID_IMAGE_DESCRIPTOR                 ),
	TO_STRING(CL_INVALID_COMPILER_OPTIONS                 ),
	TO_STRING(CL_INVALID_LINKER_OPTIONS                   ),
	TO_STRING(CL_INVALID_DEVICE_PARTITION_COUNT           ),
	TO_STRING(CL_INVALID_PIPE_SIZE                        ),
	TO_STRING(CL_INVALID_DEVICE_QUEUE                     ),
	TO_STRING(CL_INVALID_SPEC_ID                          ),
	TO_STRING(CL_MAX_SIZE_RESTRICTION_EXCEEDED            ),
};

/*****************************************************************************
 * Private API 
 ****************************************************************************/

extern char* file_to_string(const char *filepath);

void _print_device_info(cl_device_id device_id)
{
  int err;                            // error code returned from OpenCL calls
  cl_device_type device_type;         // Parameter defining the type of the compute device
  cl_uint comp_units;                 // the max number of compute units on a device
  cl_char vendor_name[1024] = {0};    // string to hold vendor name for compute device
  cl_char device_name[1024] = {0};    // string to hold name of compute device
  cl_uint          max_work_itm_dims;
  size_t           max_wrkgrp_size;
	size_t          *max_loc_size;
	cl_ulong 				mem_size;

	err = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(device_name), &device_name, NULL);
  if (clutil_check_error(err, __LINE__) < 0) {
    return;
  }

  _clinfo("%s ",device_name);
  err = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof(device_type), &device_type, NULL);
  if(device_type  == CL_DEVICE_TYPE_GPU)
     printf("GPU from ");
  else if (device_type == CL_DEVICE_TYPE_CPU)
     printf("CPU from ");
  else 
     printf("non CPU or GPU processor from ");

  err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof(vendor_name), &vendor_name, NULL);
  printf("%s ",vendor_name);

  err = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &comp_units, NULL);
  printf("\n max compute units: %d",comp_units);
  
  err = clGetDeviceInfo( device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), 
                             &max_work_itm_dims, NULL);
  max_loc_size = (size_t*)malloc(max_work_itm_dims * sizeof(size_t));
  if(max_loc_size == NULL) {
     printf(" malloc failed\n");
     return;
  }
  err = clGetDeviceInfo( device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, max_work_itm_dims* sizeof(size_t), max_loc_size, NULL);
  err = clGetDeviceInfo( device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_wrkgrp_size, NULL);
	printf("\n max # of work items at each dim per work group: ");
	for(int i=0; i< max_work_itm_dims; i++)	printf(" %d ",(int)(*(max_loc_size+i)));
	printf("\n");
	printf(" max work group size: %d\n",(int)max_wrkgrp_size);

	printf(" mem size global, local: ");
	err = clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &mem_size, NULL);
	printf("%ld, ", mem_size);
	err = clGetDeviceInfo(device_id, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &mem_size, NULL);
	printf("%ld\n", mem_size);
}

/*****************************************************************************
 * Public Types
 ****************************************************************************/
/*****************************************************************************
 * Public API 
 ****************************************************************************/

/** clutil_build_kernel
  * returns: cl_program, must be freed using clReleaseProgram(..)
  */
cl_program clutil_build_program(cl_device_id device, 
																cl_context context, 
																const char * kernel_file_path,
																const char * ccflags)
{
 	const char *str_program = file_to_string(kernel_file_path);

 	if (str_program == NULL) {
 		goto error;
 	}

 	cl_int err = 0;
 	cl_program program = clCreateProgramWithSource(context, 1, 
 		(const char **) &str_program, NULL, &err);
 	
 	if (clutil_check_error(err, __LINE__) < 0) {
 		goto error;
 	}

 	err = clBuildProgram(program, 0, NULL, ccflags, NULL, NULL);
 	char buffer[1048576];
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 
												sizeof(buffer), buffer, NULL);
  _clinfo("%s\n", buffer);
 	if (clutil_check_error(err, __LINE__) < 0) {
    goto error;
 	}

	free((void *) str_program);
 	return program;

error: 
	free((void *) str_program);
	return NULL;
}

/** load_device
  *   
  */
cl_device_id clutil_load_device(int dev_number, bool print_info)
{
	cl_int 				 err = 0;
	cl_uint 			 num_platforms = 0;
	cl_platform_id platforms[MAX_PLATFORMS];
	cl_device_id 	 devices[MAX_DEVICES];

	err = clGetPlatformIDs(MAX_PLATFORMS, platforms, &num_platforms);
	if (err < 0) {
		_clinfo("error getting platforms %d\n", err);
		return NULL;
	}

	cl_uint num_devices = 0;
	for (int i=0; i<num_platforms; i++) {
		cl_uint num = 0;
		err = clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 
			MAX_DEVICES-num_devices, devices+num_devices, &num);
		num_devices += num;
	}

	_clinfo("Platforms: %d; Devices: %d\n", num_platforms, num_devices);

	if (print_info) {
		for (int i=0; i<num_devices; i++) {
			if (i == dev_number) {
				_clinfo("Device %d (selected)\n", i);
			} else {
				_clinfo("Device %d\n", i);	
			}			
			_print_device_info(devices[i]);
		}
	}

	if (dev_number < num_devices) {
		return devices[dev_number];
	} else {
		return NULL;
	}
}

/** perror
 * returns: negative if there is an error and prints it if so
 */
int clutil_check_error(cl_int err, int line)
{
	if (err < 0) {
		int indx = -err;
		if (indx >= 30) indx -= 10; // not sure why CL errors skip 20...29
		_clinfo("%d: %s\n", line, cl_error_strings[indx]);
		return err;
	} else {
		return 0;
	}
}

/** setup
 * perform typical setup, calls clutil_load_dev with dev 0
 */
clutil_basic_params_s* clutil_basic_init(const char *kernel_file_path, 
                                         const char *kernel_main,
                                         const char *ccflags)
{
	cl_int err;

	clutil_basic_params_s *params = malloc(sizeof(clutil_basic_params_s));
	if (!params) {
		_clinfo("failed to malloc!\n");
		return NULL;
	}

	params->device = clutil_load_device(0, true);
  if (!params->device) {
    goto error;
  }

  params->context = clCreateContext(0, 1, &params->device, NULL, NULL, &err);
  if (clutil_check_error(err, __LINE__) < 0) {
  	goto error;
  }

  params->cmd_q = clCreateCommandQueueWithProperties(
								  	params->context, 
								  	params->device, 
								  	0, 
								  	&err);
  if (clutil_check_error(err, __LINE__) < 0) {
  	goto error;
  }

  params->program = clutil_build_program(
									  	params->device, 
									  	params->context, 
									  	kernel_file_path,
									  	ccflags);
  if (!params->program) {
    goto error;
  }

  params->kernel = clCreateKernel(params->program, kernel_main, &err);
  if (clutil_check_error(err, __LINE__) < 0) {
    goto error;
  }

  return params;

error:
	free(params);
	return NULL;
}

void clutil_basic_deinit(clutil_basic_params_s **params)
{
	if (!(*params)) {
		clutil_basic_params_s *priv = *params;
		clReleaseProgram(priv->program);
    clReleaseKernel(priv->kernel);
    clReleaseCommandQueue(priv->cmd_q);
    clReleaseContext(priv->context);

    free(priv);
    *params = NULL;
  }
}