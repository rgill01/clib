/****************************************************************************
	Vector library, taken from my nuttx app lib. 
	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <math.h>
#include <gsl/gsl_randist.h>
#include <stdlib.h>

#include "../dynamics.h"
#include "../utilities.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#if 0 /* debug */
#define _debug(format,...)  printf("[MAIN DEBUG %s %d] " format, __func__,__LINE__,##__VA_ARGS__)
#define _debugvec 					vec3d_print
#else 
#define _debug(format,...) do{ }while(0)
#define _debugvec(...)			
#endif

/****************************************************************************
 * Public Types
 ****************************************************************************/
/****************************************************************************
 * Public Data
 ****************************************************************************/
/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/**
 * dynamics_rigid_body: 
 * 	standard rigid body dynamics given force and torques
 *  pos, vel, and rates are all w.r.t. center of mass
 * @q: forward frame rotation (rotates frame I to B)
 */
void dynamics_rigid_body(const dynamics_rigid_body_s *rb,
	const dynamics_rb_state_s *x, const vec3d *BF, const vec3d *Btau,
	dynamics_rb_state_s *xD)
{
	/* pD = v */
	vec3d_copy(&x->vel, &xD->pos);

	/* vD = iF/m */
	vec3d IF;
	// rotd_print(__func__, &x->q);
	rotd_body2inertial(&x->q, BF, &IF);
	vec3d_div_scalar(&IF, rb->mass, &xD->vel);
	// printf("%f\n", rb->mass);
	// vec3d_print(__func__, BF);

	/* qD = 1/2 Q(q) [0 Bw] */
	rotd_rate(&x->q, &x->w, &xD->q);

	/* BwD = invJ * (Btau - Bw x J Bw) */
	vec3d JBw = { rb->inertia[0][0]*x->w[0], 
								rb->inertia[1][1]*x->w[1],
							 	rb->inertia[2][2]*x->w[2],};
	vec3d cross;
	vec3d_cross(&x->w, &JBw, &cross);

	vec3d torque_with_gyroscopic;
	vec3d_subtract(Btau, &cross, &torque_with_gyroscopic);

	for (uint8_t i=0; i<3; i++)
		xD->w[i] = torque_with_gyroscopic[i]/rb->inertia[i][i];
}

/**
 * dynamics_motor: 
 */
void dynamics_motor(const dynamics_motor_s *m,
	double w, double u_volts, double torque_load, double *wd)
{
	double torque_motor;
	constraind(&u_volts, m->u_min, m->u_max);

	torque_motor = m->K*(u_volts - m->K*w)/m->R;
	if (fabs(w) > 1) /* if omega is large enough, add stiction */
		torque_motor += w>0 ? -m->torque_stiction : m->torque_stiction;
	
	(*wd)        = 1/m->inertia * (torque_motor - torque_load);
}


/**
 * aero_propeller_force_torque
 *  @B_vinf: air velocity in the body frame, where z aligns with rotor plane
 */
void aero_propeller_force_torque(const aero_prop_s *p, 
	const vec3d *B_Vinf, double Omega, vec3d *BF, double *torqueZ)
{
	double thrust, drag, torque;

	/* compute angle Beta. V_inf is air velocity, have to negate to get equivalent
	 * body velocity
	 */
	double Vinf = vec3d_norm(B_Vinf);
	double beta;
	beta = (Vinf > 1e-3) ? safeacosd( -(*B_Vinf)[2]/Vinf ) : 0;
	_debug("beta = %f\n", beta);
	/* mu = Vinf sin beta / omega R */
	double mu, lc;
	if (Omega < 1e-3) {
		mu = lc = 0;
	} else {
		mu   = Vinf * sin(beta) / (Omega * p->R);
		lc   = Vinf * cos(beta) / (Omega * p->R);
	}	
	
	/* thrust */
	double Ct = p->ct_0 + p->ct_mu2*squared(mu) + p->ct_lc*lc + p->ct_lc2*squared(lc);	
	double A  = M_PI*squared(p->R);
	thrust    = Ct * (0.5*p->rho*A*squared(Omega*p->R));

	/* rotor drag */
	double Crd = p->crd_mu*mu;
	drag       = Crd * (0.5*p->rho*A*squared(Omega*p->R));

	/* torque */
	double Cq = p->cq_0 + p->cq_mu2*squared(mu) + p->cq_lc*lc + p->cq_lc2*squared(lc);
	torque    = Cq * (0.5*p->rho*A*squared(Omega*p->R)*p->R);

	/* output result */
	(*torqueZ) = torque;
	vec3d rotor_drag = {(*B_Vinf)[0], (*B_Vinf)[1], 0};
	if (vec3d_norm(&rotor_drag) > 1e-3){
		vec3d_normalize(&rotor_drag);
		vec3d_mult_scalar(&rotor_drag, drag, &rotor_drag);
	}
	vec3d_copy(&rotor_drag, BF);
	(*BF)[2]   += thrust;
}

/**
 * aero_annular_wing_loads:
 * 	@B_vinf: air velocity (or negated body velocity if no wind) in the 
 * 					body frame
 */
void aero_annular_wing_loads(const aero_annular_wing_s *aw,
	const vec3d *B_Vinf, const double (*cf_turb)[3], vec3d *BF, vec3d *BTau)
{
	double lift, drag, moment;

	double V   = vec3d_norm(B_Vinf);
	double Vsq = vec3d_norm_sq(B_Vinf);

	/* construct airframe */
	vec3d Bi_a, Bj_a, Bk_a;
	/* i_a = Vinf normalized */
	if (V > 1e-3) {
		vec3d_div_scalar(B_Vinf, V, &Bi_a);
	} else {
		vec3d_copy(&vec3d_i, &Bi_a);
	}
	/* j_a = k_b cross Vinf */
	Bj_a[0] = -Bi_a[1];
	Bj_a[1] =  Bi_a[0];
	Bj_a[2] = 0;
	vec3d_normalize(&Bj_a); // todo: if vinf parallel to k_b..
	/* k_a = i_a cross j_a */
	vec3d_cross(&Bi_a, &Bj_a, &Bk_a);

	_debugvec("Bi_a", &Bi_a);
	_debugvec("Bj_a", &Bj_a);
	_debugvec("Bk_a", &Bk_a);

	/* lift acts along k_a, drag along i_a, moment along j_a */
	
	/* compute aoa */
	double aoa = (V > 1e-3) ? safeacosd( -(*B_Vinf)[2]/V ) : 0; /* between 0 and pi */
	_debug("aoa = %f\n", aoa);

	double CL;
	/* CL */
	if (aoa < M_PI_2) {
		CL = mind(aw->cla[0]*aoa+aw->clb[0], 
			mind(aw->cla[1]*aoa+aw->clb[1],aw->cla[2]*aoa+aw->clb[2]));
	} else {
		double aoa_p = M_PI-aoa;
		CL = -mind(aw->cla[0]*aoa_p+aw->clb[0], 
			mind(aw->cla[1]*aoa_p+aw->clb[1],aw->cla[2]*aoa_p+aw->clb[2]));
	}
	/* CD */
	double CD;
	double aoa_b = (aoa < M_PI_2) ? aoa : M_PI - aoa;
	CD = mind(aw->cda[0]*aoa_b+aw->cdb[0], aw->cda[1]*aoa_b+aw->cdb[1]);

	/* CM */
	double CM;
	CM = aw->cma*sin(aoa);

	/* turbulence */
	CL += (*cf_turb)[0];
	CD += (*cf_turb)[1];
	CM += (*cf_turb)[2];
	
	_debug("Cl = %f, Cd = %f, Cm = %f\n", CL, CD, CM);
	
	lift       = CL * (0.5*aw->rho*Vsq*aw->c*aw->d);
	drag       = CD * (0.5*aw->rho*Vsq*aw->c*aw->d);
	moment     = CM * (0.5*aw->rho*Vsq*aw->c*aw->d)*aw->c;

	/* rotate back to body frame */
	vec3d_mult_scalar(&Bi_a, drag, BF);
	vec3d_add_s_mult(BF, &Bk_a, lift, BF);
	vec3d_mult_scalar(&Bj_a, moment, BTau);
}

/**
 * aero_turbulence_dynamics: 
 * 	continous time dynamics function for turbulence, modeled simply as coloured
 *  noised by a first-order differential equation driven by 
 *  zero-mean, unit-variance noise eta.
 *  @turb_state: state of the turbulence 
 *  @variate of eta, unit-variance, zero-mean
 */
double aero_turbulence_dynamics(const aero_turbulence_ar_s *at, 
	double turb_state, double eta)
{
	return eta*at->std - turb_state/at->tau;
}