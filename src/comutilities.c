#include <termios.h>
#include <stdio.h>
#include "../comutilities.h"

int comutil_set_attributes(int fd, int speed, int parity, int should_block, int read_size)
{
	struct termios tty = {0};
  if (tcgetattr (fd, &tty) != 0) {
    perror("Error retrieving term attr");
    return -1;
  }

  /* convert baud rate to Bnnn value define din termios.h */
  switch (speed)
  {
    case 115200:
      speed = B115200;
      break;
    default:
      return -1;
  }

  cfmakeraw(&tty);
  cfsetospeed (&tty, speed);
  cfsetispeed (&tty, speed);
  
  tty.c_cc[VMIN]  = read_size;            // blocking read. see pg 703 unix manual
  tty.c_cc[VTIME] = should_block ? 0 : 5; // 0.5 seconds read timeout if block not set

  if (tcsetattr (fd, TCSANOW, &tty) != 0) {
		perror("Error setting term attr");
		return -1;
  }
  return 0;
}



int comutil_flush_input(int fd)
{
  return tcflush(fd, TCIFLUSH);
}