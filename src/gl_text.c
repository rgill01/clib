#include "../glutilities.h"
#include <stdbool.h>
#include <stdio.h>
#include <freetype2/ft2build.h>
#include "../utilities.h"


#include FT_FREETYPE_H

#define FONT_TFF 			"/usr/share/fonts/truetype/clear-sans/ClearSans-Bold.ttf"
#define ASCII_OFFSET  32

typedef struct glutil_text_s
{
	GLuint vbo,
				 cbo,
				 vao,
				 n;
} glutil_text;


/* globals */

static GLuint shader, tex_id;
static bool texture_init;

static FT_Library ft;
static FT_Face face;
static struct character_info {
	uint16_t	ax,  /* advance.x [px] */
						bw,  /* bitmap.width [px] */
						bh,  /* bitmap.rows [px] */
						bt,  /* bitmap.top [px] */
						tx,  /* x offset of glyph in tecture coordinates [openGL] */
						ty;  /* y offset of glpyh in texture coordinates [openGL] */
} c[128-ASCII_OFFSET] = {{0}};

/*****************************************************************************
 * Private API
 ****************************************************************************/

static inline GLshort px_to_glshort(int px, float offset, int w)
{
	return ((float) px/w - offset)*UINT16_MAX;
}

static void create_atlas(int fontsize_px)
{
	unsigned int w = 0, h = 0, i, x = 0;

	if(FT_Init_FreeType(&ft)) {
		fprintf(stderr, "Could not init freetype library\n");
		return;
	}
	if(FT_New_Face(ft, FONT_TFF, 0, &face)) {
	  fprintf(stderr, "Could not open font\n");
	  return;
	} 

	FT_Set_Pixel_Sizes(face, 0, fontsize_px);	

	/* find max dimensions of glpyhs */

	FT_GlyphSlot g = face->glyph;
	for (i=32; i < 128; i++) {
		if(FT_Load_Char(face, i, FT_LOAD_RENDER)) {
	    fprintf(stderr, "Loading character %d:%c failed!\n", i,i);
	    continue;
  	}
  	w += g->bitmap.width;
  	h = maxui(h, g->bitmap.rows);
	}

	/* upload the texture */

	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex_id);
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(
		GL_TEXTURE_2D, 
		0, 
		GL_RED, 
		w, 
		h, 
		0, 
		GL_RED, 
		GL_UNSIGNED_BYTE, 
		0);

	/* w is position in bitmap units along the texture */

	for (i=32; i<128; i++) {
		if(FT_Load_Char(face, i, FT_LOAD_RENDER)){
    	continue;
  	}
  	glTexSubImage2D(
  		GL_TEXTURE_2D, 
  		0, 
  		x, 
  		0, 
  		g->bitmap.width, 
  		g->bitmap.rows, 
  		GL_RED, 
  		GL_UNSIGNED_BYTE, 
  		g->bitmap.buffer);
		
		c[i-ASCII_OFFSET].tx = ((float) x/w) * INT16_MAX;
		c[i-ASCII_OFFSET].ty = ((float) g->bitmap.rows/h) * INT16_MAX;
		c[i-ASCII_OFFSET].ax = g->advance.x >> 6;
		c[i-ASCII_OFFSET].bw = g->bitmap.width;
		c[i-ASCII_OFFSET].bh = g->bitmap.rows;
		c[i-ASCII_OFFSET].bt = g->bitmap_top;

		x += g->bitmap.width;
	}

	/* set up texture settings */

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* shader programs */

	const char *vertex_shader_c = 
		"#version 330 core\n"
		"layout(location=0) in vec4 coord;\n"
		"layout(location=1) in vec3 colour;\n"
		"out vData { vec2 texcoord; vec3 c; };\n"
		"void main(void) { \n"
		"  gl_Position = vec4(coord.xy, 0, 1); \n"
		"  texcoord = coord.zw; \n"
		"  c  = colour;}";

	const char *frag_shader_c = 
		"#version 330 core\n"
		"in vData {vec2 texcoord; vec3 c;};\n"
		"uniform sampler2D tex;\n"
		"out vec4 frag_colour;\n"
		"void main(void) { \n"
		"  frag_colour = vec4(c.x,c.y,c.z, texture2D(tex, texcoord).r);}";

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(vs, 1, &vertex_shader_c, NULL);
	glShaderSource(fs, 1, &frag_shader_c, NULL);
	glCompileShader(vs);
	glCompileShader(fs);

	/* Check shader */
	int InfoLogLength;
	GLint result;
	glGetShaderiv(fs, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 )
	{
		char shader_log[InfoLogLength];
		glGetShaderInfoLog(fs, InfoLogLength, NULL, shader_log);
		printf("%s\n", shader_log);
	}

	shader = glCreateProgram();
	glAttachShader(shader, vs);
	glAttachShader(shader, fs);
	glLinkProgram(shader);

		// Check the program
	glGetProgramiv(shader, GL_LINK_STATUS, &result);
	glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		char program_log[InfoLogLength];
		glGetProgramInfoLog(shader, InfoLogLength, NULL, program_log);
		printf("%s\n", program_log);
	}

	glDetachShader(shader, vs);
	glDetachShader(shader, fs);
	glDeleteShader(vs);
	glDeleteShader(fs);

  glUseProgram(shader);

  // u_color_id = glGetUniformLocation(shader, "colour");
  // glUniform4f(u_color_id, 0,0,0,1);

  texture_init = true;
}

/*****************************************************************************
 * Public API
 ****************************************************************************/

/** initiate gl buffer for holding text data
  * uses global texture for all instances
  * instance is used to keep track of all rendered text
  */
glutil_text* glutil_text_initiate(int fontsize_px)
{
	if (!texture_init) {
		create_atlas(fontsize_px);
	}
	glutil_text *t = calloc(1, sizeof(glutil_text));
	if (t == NULL) return NULL;


	glUseProgram(shader);

	/* create the gpu buffer */

	glGenVertexArrays(1, &t->vao);
	glBindVertexArray(t->vao);

	glGenBuffers(1, &t->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, t->vbo);
	glVertexAttribPointer(0, 4, GL_SHORT, GL_TRUE, 0, 0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &t->cbo);
	glBindBuffer(GL_ARRAY_BUFFER, t->cbo);
	glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE, 0, 0);
	glEnableVertexAttribArray(1);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return t;
}

/** renders text, puts it onto gpu 
  */
void glutil_text_render(glutil_text* t, const glutil_text_data *in, int n_strings, int ww, int wh)
{
	const char *p;
	uint8_t i,j;
	int n_chars = 0, px, n = 0;

	for (j=0; j<n_strings; j++) {
		n_chars += strlen(in[j].text);
	}

	struct point {
		GLshort x, y, s, t;
	} coords[6 * n_chars]; /* 2d position, 2d texture coord, 6 vertex per char */

	struct colour {
		GLbyte r, g, b;
	} colours[6 * n_chars];

	for (j=0; j<n_strings; j++){
		px = in[j].px;
		for (p = in[j].text; *p; p++) {
			i = (uint8_t) *p - ASCII_OFFSET;
	
			/* compute coordinates in openGL frame */
			GLshort x =  px_to_glshort(px, 0.5, ww),
						  y =  px_to_glshort(-(in[j].py - c[i].bt), -0.5, wh),
							w =  px_to_glshort(c[i].bw, 0, ww),
							h =  px_to_glshort(c[i].bh, 0, wh);

			/* advance cursor */
			px += c[i].ax;

			if (!w || !h) continue;
					
			coords[n++] = (struct point){x, y, c[i].tx, 0};
			coords[n++] = (struct point){x+w, y, c[i+1].tx, 0};
			coords[n++] = (struct point){x, y-h, c[i].tx, c[i].ty};
			coords[n++] = (struct point){x+w, y, c[i+1].tx, 0};
			coords[n++] = (struct point){x, y-h, c[i].tx, c[i].ty};
			coords[n++] = (struct point){x+w, y-h, c[i+1].tx, c[i].ty};

			//TODO: this seems stupid, use uniform colour? but then have to 
			// save colours, and in draw, need to switch between all different 
			// texts, bind colour, draw array, 
			for (int k = 1; k <= 6; k ++) 
				colours[n-k] = (struct colour){in[j].r, in[j].g, in[j].b};
		}
	}
	t->n = n;
	glBindBuffer(GL_ARRAY_BUFFER, t->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(coords), coords, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, t->cbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colours), colours, GL_DYNAMIC_DRAW);
}

/** tells gpu to draw. this is fast, just switches to instance veretx array obj 
  * and draws. No copying done.
  */
void glutil_text_draw(glutil_text* t)
{
	/* set up */
	glEnable(GL_BLEND);
	glUseProgram(shader);
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBindVertexArray(t->vao);

	glDrawArrays(GL_TRIANGLES, 0, t->n);
	
	/* clean up */
	glBindTexture(GL_TEXTURE_2D, 0); 
	glDisable(GL_BLEND);
}

void glutil_text_destroy(glutil_text *t)
{
	if (texture_init) {
	  FT_Done_Face    ( face );
	  FT_Done_FreeType( ft );
	  glDeleteShader(shader);
	  glDeleteTextures(1, &tex_id);

	  texture_init = false;
	}
	glDeleteBuffers(1, &t->vbo);
	glDeleteVertexArrays(1, &t->vao);
	free(t);
}