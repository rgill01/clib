#include "../glutilities.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef DEBUG
	#define glinfo(x...) 	printf("[GLutil info]" x)
#else
	#define glinfo(x...)
#endif


char* file_to_string(const char *filepath)
{
	/* Read the file and put it into string on the heap */

	FILE *f = fopen(filepath, "r");
	if (f==NULL)
	{
		fprintf(stderr, "Cannot open shader file.\n");
		return 0;
	}

	fseek(f, 0, SEEK_END);
	int len = ftell(f);
	fseek(f, 0, SEEK_SET);
	char* s = (char *) malloc(len+1);
	if (s == NULL) {
		fprintf(stderr, "malloc failed\n");
	}
	
	size_t ret = fread(s, 1, len, f);
	if (ret <= 0) {
		fprintf(stderr, "Cannod read file %s.\n", filepath);
		free(s);
		s = 0;
	}

	s[len] = '\0';
	fclose(f);
	glinfo("string:\n<<%s>>", s);
	return s;
}

static GLint compile_shader(const char* const *code, GLuint shaderID)
{
	GLint result = GL_FALSE;
	int InfoLogLength;

	glShaderSource(shaderID, 1, code , NULL);
	glCompileShader(shaderID);

	/* Check shader */

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ) {
		char shader_log[InfoLogLength];
		glGetShaderInfoLog(shaderID, InfoLogLength, NULL, shader_log);
		glinfo("%s\n", shader_log);
	}

	return result;
}

GLuint glutil_shaders_load(const char * vertex_file_path, const char * fragment_file_path)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const char *vertexShaderCode = file_to_string(vertex_file_path);
	const char *fragmentShaderCode = file_to_string(fragment_file_path);

	if (vertexShaderCode == NULL ||
		  fragmentShaderCode == NULL)
		goto error;

	GLint result, InfoLogLength;

	// Compile Vertex Shader
	glinfo("Compiling shader : %s\n", vertex_file_path);
	if (!compile_shader(&vertexShaderCode, VertexShaderID))
		goto error;

	// Compile Fragment Shader
	glinfo("Compiling shader : %s\n", fragment_file_path);
	if (!compile_shader(&fragmentShaderCode, FragmentShaderID))
		goto error;

	// Link the program
	glinfo("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		char program_log[InfoLogLength];
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, program_log);
		glinfo("%s\n", program_log);
	}
	
	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);
	
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	free((char *) vertexShaderCode);
	free((char *) fragmentShaderCode);

	return ProgramID;

error:
	free((char *) vertexShaderCode);
	free((char *) fragmentShaderCode);

	return -1;	
}