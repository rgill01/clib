#include "../tmrutilities.h"
#include <sys/timerfd.h>
#include <unistd.h>
#include <stdio.h>


int tmrutil_start (unsigned int period_ms, struct tmrutil_info *info)
{
	unsigned long sec = period_ms/1e3,
								ns  = period_ms*1e6 - sec*1e9;
	struct itimerspec itim = { {sec, ns}, {sec, ns} };

	if ( (info->timer_fd = timerfd_create(CLOCK_MONOTONIC, 0)) < 0) {
		perror("timerfd_create");
		return info->timer_fd;
	}
	info->wakeups_missed = 0;
	return timerfd_settime(info->timer_fd, 0, &itim, NULL);
}

void tmrutil_wait (struct tmrutil_info *info)
{
	unsigned long long missed;

	/* Wait for the next timer event. If we have missed any the
	   number is written to "missed" */
	if (read (info->timer_fd, &missed, sizeof (missed)) <0) {
		perror ("read timer");
		return;
	}
	/* "missed" should always be >= 1, but just to be sure, check it is not 0 anyway */
	if (missed > 0)
		info->wakeups_missed += (missed - 1);
}


void tmrutil_close(struct tmrutil_info *info)
{
	printf("No. of missed wakeups = %lld\n", info->wakeups_missed);
	close(info->timer_fd);
}