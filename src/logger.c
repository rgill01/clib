/****************************************************************************
	Spawn a low priority logging thread that writes to the hard disk floats it 
	receives.

	Copyright (C) 2017  Rajan Gill

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "logger.h"

#include <errno.h>
//#include <mqueue.h>  /* push message to logger thread */
#include <fcntl.h>   /* open file */
//#include <pthread.h> /* spawn thread */
#include <stdio.h>   /* file io */
#include <dirent.h>  /* for renaming log file using # of log files */
//#include <time.h>    /* for clock realtime, struct, mq receive timeout */
#include <stdbool.h>
#include <string.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define LOG_FILE_DIR      "log/"
#define LOG_FILE_NAME 		"log.dat"
#define DTYPE_FILE 				DT_REG
#define LOG_ERROR()				perror("LOGGER: ")

/****************************************************************************
 * Private Data
 ****************************************************************************/

static uint16_t						 g_n_floats = 0;
static FILE  							 *log_file = NULL;

/****************************************************************************
 * Private Function Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Function Definitions
 ****************************************************************************/

/****************************************************************************
 * log_init
 *
 *  Description:
 *  	initialize logger.. call from spawning thread
 *      
 ****************************************************************************/

int log_push(uint32_t t, const float *f)
{
	if (fwrite(&t, sizeof(uint32_t), 1, log_file) != 1)
		goto error;
	if (fwrite(f, sizeof(float), g_n_floats, log_file) != g_n_floats)
		goto error;

	return 0;
error:
	LOG_ERROR();
	return -1;
}

/****************************************************************************
 * log_init
 *
 *  Description:
 *  	initialize logger.. call from spawning thread
 *      
 ****************************************************************************/

int log_init(uint16_t no_of_floats)
{
	g_n_floats = no_of_floats;

	/* init log file */

	log_file = fopen(LOG_FILE_DIR LOG_FILE_NAME, "wb");
	if (!log_file) {
		printf("LOGGER: Failed to open " LOG_FILE_DIR LOG_FILE_NAME "\n");
		return -1;
	}
	return 0;
}

/****************************************************************************
 * log_deinit
 *
 *  Description:
 *  	destroy log thread
 *      
 ****************************************************************************/

void log_deinit(void)
{
	if (log_file == NULL)
		return;

	fclose(log_file);
		
	/* rename log file */

	uint16_t file_count = 0;
  DIR * dirp; struct dirent *dir_entry; 
  dirp = opendir(LOG_FILE_DIR);
  if (dirp)
  {
    while ( (dir_entry = readdir(dirp)) != NULL )
    {
      if (dir_entry->d_type == DTYPE_FILE)
        file_count++;
    }
    char filename[32] = LOG_FILE_DIR "log_", count_str[11];
    sprintf(count_str, "%02d", file_count);
	  strcat(filename, count_str);//itoa(file_count, count_str, 10));
	  strcat(filename, ".dat");
	  rename(LOG_FILE_DIR LOG_FILE_NAME, filename);
	  printf("LOGGER: Log file saved as %s\n", filename);
	  closedir(dirp);
  }
  else
  {
    printf("LOGGER: Opening directory " LOG_FILE_DIR " failed for file rename.\n");
  }
}