#ifndef __COMMON_GL_UTIL_H_
#define __COMMON_GL_UTIL_H_

#ifdef USE_GLEW /* hack so I can use easyclang */
    #include <GL/glew.h>
#else
    #warning "Not using GLEW"
    #define GL_GLEXT_PROTOTYPES
    #include <GL/gl.h>
    #include <GL/glext.h>
#endif


/*****************************************************************************
 * Public Types
 ****************************************************************************/

typedef struct glutil_text_s glutil_text;

typedef struct {
  const char *text;
  int px, py;         /* window pixel location */
  uint8_t r, g, b;    /* in [0,255] */
} glutil_text_data;

/*****************************************************************************
 * Public API 
 ****************************************************************************/

/* shaders related */

/** util_loadshaders
  * returns: program id 
  */
GLuint glutil_shaders_load(const char * vertex_file_path, 
													 const char * fragment_file_path);


/** initiate gl buffer for holding text data
  * uses global texture for all instances
  * instance is used to keep track of all rendered text
  */
glutil_text* glutil_text_initiate(int fontsize_px);

/** renders text, puts it onto gpu 
  */
void glutil_text_render(glutil_text* t, const glutil_text_data *in, int n, int ww, int wh);

/** tells gpu to draw. this is fast, just switches to instance veretx array obj 
  * and draws. No copying done.
  */
void glutil_text_draw(glutil_text* t);

/** clean up text stuff
  */
void glutil_text_destroy(glutil_text *t);

#endif /* include guard */