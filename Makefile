DEBUG   = 0
CC 			= gcc
AR 			= ar
CFLAGS  = `pkg-config --cflags freetype2` -Wall  -fPIC -D USE_GLEW -I .
ifeq ($(DEBUG), 1)
	CFLAGS += -O -g  # "-O" is needed to expand inlines
else
	CFLAGS += -O3 
endif

LFLAGS  = `pkg-config --libs freetype2` -lGL -lGLEW -lgsl -lopenblas -lOpenCL

LOBJS 	 = $(patsubst %.c,%.o,$(wildcard src/*.c))
LIB 		 = libmyclib.so

INSTALL_DIR = /usr

$(LIB): $(LOBJS)
	$(CC) -shared -o $@ $(LOBJS) $(LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

install: $(LIB)
	install -m 0444 $(LIB) $(INSTALL_DIR)/lib
	mkdir $(INSTALL_DIR)/include/myclib
	install -m 0444 *.h $(INSTALL_DIR)/include/myclib/

clean:
	rm -f $(LOBJS) $(LIB)